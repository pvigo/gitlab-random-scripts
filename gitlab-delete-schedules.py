#!/usr/bin/python3
import logging
import sys

import gitlab
import requests  # noqa: F401, needed to get the logger

logging.basicConfig()
logging.getLogger().setLevel(logging.DEBUG)
req_log = logging.getLogger("requests.packages.urllib3")
req_log.setLevel(logging.DEBUG)
req_log.propagate = True

gl = gitlab.Gitlab.from_config("apertis")
gl.auth()

project_full_path = sys.argv[1]

project = gl.projects.get(project_full_path, lazy=True)

schedules = project.pipelineschedules.list(all=True)
schedules.sort(key=lambda x: x.id)

update_schedules = [p for p in schedules if p.ref == "debian/buster-gitlab-update-job"]

if not update_schedules:
    print(f"{project_full_path} no pipelines")
else:
    for p in update_schedules:
        print(f"{project_full_path} delete {p.ref} {p.cron} {p.description}")
        p.delete()
