#!/usr/bin/env python3

import argparse
import concurrent.futures
import fnmatch
import logging

import gitlab


def thread_pool(num_workers, func, items, num_retries=0):
    def func_with_retries(item, counter, total):
        for i in range(1, num_retries + 2):
            try:
                func(item)
            except Exception:
                if i == num_retries + 1:  # this was the last retry
                    raise
                logging.debug("Retry")
            else:
                break

    with concurrent.futures.ThreadPoolExecutor(max_workers=num_workers) as executor:
        items = list(items)
        total = len(items)
        futures = [
            executor.submit(func_with_retries, item, counter, total)
            for counter, item in enumerate(items, start=1)
        ]
    results = [f.result() for f in futures]
    return results


def is_glob(pattern):
    return any(i in pattern for i in ["*", "?", "["])


def _project_match_filter(project, filterglob):
    if not filterglob:
        return True
    return fnmatch.fnmatch(project.path_with_namespace, filterglob)


class Lister:
    def __init__(self):
        self.gl = None
        self.projects = None
        self.pipelines = None

    def connect(self, gitlab_instance):
        logging.info(f'Connecting to the "{gitlab_instance}" configured instance')
        self.gl = gitlab.Gitlab.from_config(gitlab_instance)
        self.gl.auth()

    def fetch_projects(self, filterglob):
        if filterglob and not is_glob(filterglob):
            self.projects = [self.gl.projects.get(filterglob)]
            return

        num_worker_threads = 10
        per_page = 100
        projects = []

        total_pages = self.gl.projects.list(
            as_list=False, per_page=per_page, simple=True
        ).total_pages

        def _fetch_page(page):
            p = self.gl.projects.list(per_page=per_page, page=page, simple=True)
            projects.extend(p)
            logging.info(f"Retrieved page {page} of {total_pages}")

        thread_pool(
            num_worker_threads, _fetch_page, range(1, total_pages + 1), num_retries=2
        )
        projects = [p for p in projects if _project_match_filter(p, filterglob)]
        projects.sort(key=lambda p: p.path_with_namespace)
        self.projects = projects

    def fetch_pipelines(self, filters: dict[str, str]):
        num_worker_threads = 10
        per_page = 100
        pipelines = {}

        def _fetch_pipelines(project):
            logging.info(f"{project.path_with_namespace}: Retrieving pipelines")
            try:
                project_pipelines = project.pipelines.list(
                    get_all=True, per_page=per_page, **filters
                )
                logging.info(
                    f"{project.path_with_namespace}: Retrieved {len(project_pipelines)} pipelines for project {project.path_with_namespace}"
                )
                if project_pipelines:
                    pipelines[project] = project_pipelines
            except Exception as e:
                print(
                    f"{project.path_with_namespace}: Exception while retrieving pipelines: {e}"
                )

        thread_pool(num_worker_threads, _fetch_pipelines, self.projects, num_retries=2)
        self.pipelines = pipelines


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--debug",
        action="store_const",
        dest="loglevel",
        const=logging.DEBUG,
        help="print debug information",
    )
    parser.add_argument(
        "--quiet",
        action="store_const",
        dest="loglevel",
        const=logging.WARNING,
        help="do not print informational output",
    )
    parser.add_argument(
        "--gitlab-instance",
        type=str,
        default="apertis",
        help="get connection parameters from this configured instance",
    )
    parser.add_argument(
        "--filter",
        type=str,
        required=True,
        help="only act on projects matching the specified path glob",
    )
    parser.add_argument(
        "--status",
        type=str,
        help="filter pipelines by status",
    )
    parser.add_argument(
        "--updated-before",
        type=str,
        help="filter pipelines with their last update before a specific date",
    )
    parser.add_argument(
        "--retry",
        type=str,
        help="pattern for ref of failed pipelines to retry",
    )
    parser.add_argument(
        "--cancel",
        type=str,
        help="pattern for ref of pipelines to cancel",
    )
    args = parser.parse_args()

    logging.basicConfig(level=args.loglevel or logging.INFO)

    lister = Lister()
    lister.connect(args.gitlab_instance)
    lister.fetch_projects(args.filter)
    assert lister.projects
    print(f"Got {len(lister.projects)} projects")
    filters = {
        "updated_before": args.updated_before,
        "status": args.status,
    }
    lister.fetch_pipelines(filters)
    print(
        f"Got {sum(len(pipelines) for pipelines in lister.pipelines.values())} pipelines"
    )
    icons = {
        "success": "✅",
        "skipped": "»",
        "failed": "🛑",
        "pending": "⏲️",
        "running": "⚙️ ",
        "manual": "⏸️ ",
        "canceled": "𐄂",
    }
    for project, pipelines in sorted(
        lister.pipelines.items(), key=lambda p: p[0].path_with_namespace
    ):
        if not pipelines:
            continue
        print(project.path_with_namespace, project.web_url)
        for pipeline in sorted(pipelines, reverse=True, key=lambda p: p.created_at):
            action = ""
            if (
                pipeline.status == "failed"
                and args.retry
                and fnmatch.fnmatch(pipeline.ref, args.retry)
            ):
                try:
                    pipeline.retry()
                    action = "♻️ "
                except gitlab.exceptions.GitlabJobRetryError as e:
                    action = f"♻️  ({e.response_code} {e.error_message})"
            if args.cancel and fnmatch.fnmatch(pipeline.ref, args.cancel):
                try:
                    pipeline.cancel()
                    action = "❌"
                except gitlab.exceptions.GitlabJobRetryError as e:
                    action = f"❌ ({e.response_code} {e.error_message})"
            print(
                "  ",
                pipeline.ref,
                pipeline.status,
                icons.get(pipeline.status, ""),
                pipeline.web_url,
                pipeline.created_at,
                action,
            )
