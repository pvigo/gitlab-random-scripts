#!/usr/bin/env python3

import logging
import os
import queue
import sys
import threading

import gitlab

logging.basicConfig()
logging.getLogger().setLevel(logging.DEBUG)
req_log = logging.getLogger("requests.packages.urllib3")
req_log.setLevel(logging.DEBUG)
req_log.propagate = True


def graphql_projects_list(gl, group_path, cursor=None):
    print("cursor", cursor)
    query = """
  query ($cursor: String, $groupPath: ID!) {
    group(fullPath: $groupPath) {
      projects(includeSubgroups:true, after:$cursor) {
        pageInfo {
          hasNextPage
          hasPreviousPage
          endCursor
        }
        edges {
          node {
            fullPath
          }
        }
      }
    }
  }
  """
    print(query)
    data = {"query": query, "variables": {"groupPath": group_path, "cursor": cursor}}
    result = gl.http_post(gl.url + "/api/graphql", post_data=data)
    print(
        [
            e["node"]["fullPath"]
            for e in result.json()["data"]["group"]["projects"]["edges"]
        ]
    )
    return result.json()


def graphql_projects_list_all(gl, group_path):
    result = []
    r = graphql_projects_list(gl, group_path)
    result += [e["node"]["fullPath"] for e in r["data"]["group"]["projects"]["edges"]]
    print("data", r["data"])
    print("hasNextPage", r["data"]["group"]["projects"]["pageInfo"]["hasNextPage"])
    while r["data"]["group"]["projects"]["pageInfo"]["hasNextPage"]:
        cursor = r["data"]["group"]["projects"]["pageInfo"]["endCursor"]
        r = graphql_projects_list(gl, group_path, cursor)
        result += [
            e["node"]["fullPath"] for e in r["data"]["group"]["projects"]["edges"]
        ]
        print("hasNextPage", r["data"]["group"]["projects"]["pageInfo"]["hasNextPage"])
        print("endCursor", r["data"]["group"]["projects"]["pageInfo"]["endCursor"])
    return result


def thread_pool(num_workers, func, items):
    q = queue.SimpleQueue()

    def worker():
        while True:
            item = q.get()
            if item is None:
                break
            func(item)

    threads = []
    for i in range(num_workers):
        t = threading.Thread(target=worker)
        t.start()
        threads.append(t)

    for i in items:
        q.put(i)
    for t in threads:
        q.put(None)  # send sentinels
    for t in threads:
        t.join()  # wait for threads to quit after receiving a sentinel each


def threaded_projects_lists_all(gl):
    num_worker_threads = 10
    per_page = 100
    projects = []

    def func(page):
        p = gl.projects.list(query_data={"simple": True}, per_page=per_page, page=page)
        projects.extend(p)

    total_pages = gl.projects.list(
        query_data={"simple": True}, iterator=True, per_page=100
    ).total_pages
    thread_pool(num_worker_threads, func, range(1, total_pages + 1))

    return list(sorted(p.path_with_namespace for p in projects))


def iterator_projects_lists_all(gl):
    projects = gl.projects.list(
        as_list=False, obey_rate_limit=False, all=True, per_page=100
    )
    return [p.path_with_namespace for p in projects]


def basic_projects_lists_all(gl):
    projects = gl.projects.list(
        iterator=True, obey_rate_limit=False, all=True, per_page=100
    )
    return [p.path_with_namespace for p in projects]


def httplist_projects_lists_all(gl):
    projects = gl.http_list(
        "/projects", {"simple": True}, iterator=True, all=True, per_page=100
    )
    return [p["path_with_namespace"] for p in projects]


def httplistiterator_projects_lists_all(gl):
    projects = gl.http_list("/projects", {"simple": True}, iterator=True, per_page=100)
    return [p["path_with_namespace"] for p in projects]


if __name__ == "__main__":
    gl = gitlab.Gitlab.from_config(
        "apertis",
        [
            os.path.expanduser("~/.python-gitlab.cfg"),
            # FIXME add xdg dirs
        ],
    )
    kind = sys.argv[1] if len(sys.argv) > 1 else None
    if kind == "graphql":
        projects = graphql_projects_list_all(gl, "pkg")
    elif kind == "threaded":
        projects = threaded_projects_lists_all(gl)
    elif kind == "httplist":
        projects = httplist_projects_lists_all(gl)
    elif kind == "httplistiterator":
        projects = httplistiterator_projects_lists_all(gl)
    elif kind == "iterator":
        projects = iterator_projects_lists_all(gl)
    else:
        projects = basic_projects_lists_all(gl)
    for i, p in enumerate(projects):
        print(i, p)
