#!/usr/bin/env python3

import argparse
import collections
import concurrent.futures
import fnmatch
import logging

import gitlab


def thread_pool(num_workers, func, items, num_retries=0):
    def func_with_retries(item, counter, total):
        for i in range(1, num_retries + 2):
            try:
                func(item)
            except Exception:
                if i == num_retries + 1:  # this was the last retry
                    raise
                logging.debug("Retry")
            else:
                break

    with concurrent.futures.ThreadPoolExecutor(max_workers=num_workers) as executor:
        items = list(items)
        total = len(items)
        futures = [
            executor.submit(func_with_retries, item, counter, total)
            for counter, item in enumerate(items, start=1)
        ]
    results = [f.result() for f in futures]
    return results


def _project_match_filter(path_with_namespace, filterglob):
    if not filterglob:
        return True
    return fnmatch.fnmatch(path_with_namespace, filterglob)


def is_glob(pattern):
    return any(i in pattern for i in ["*", "?", "["])


class Poker:
    def __init__(self):
        self.gl = None
        self.projects = None

    def connect(self, gitlab_instance):
        logging.info(f'Connecting to the "{gitlab_instance}" configured instance')
        self.gl = gitlab.Gitlab.from_config(gitlab_instance)
        self.gl.auth()

    def _project_match_filter(self, project, filterglob):
        return fnmatch.fnmatch(project.path_with_namespace, filterglob)

    def fetch_projects(self, filterglob=None, after=None, **kwargs):
        if filterglob and not is_glob(filterglob):
            self.projects = [self.gl.projects.get(filterglob)]
            logging.debug(f"Loaded project {filterglob}")
            return
        num_worker_threads = 10
        per_page = 100
        projects = []

        def fetch_page(page):
            p = self.gl.projects.list(per_page=per_page, page=page, **kwargs)
            projects.extend(p)

        total_pages = self.gl.projects.list(
            iterator=True, per_page=100, **kwargs
        ).total_pages
        thread_pool(
            num_worker_threads, fetch_page, range(1, total_pages + 1), num_retries=2
        )
        if filterglob:
            projects = (
                p for p in projects if self._project_match_filter(p, filterglob)
            )
        if after:
            projects = (p for p in projects if p.path_with_namespace > after)
        projects = list(projects)
        logging.debug(f"Loaded {len(projects)} projects")
        projects.sort(key=lambda p: p.path_with_namespace)
        self.projects = projects

    def _matching_branches(self, project, filterglob):
        if not is_glob(filterglob):
            return [filterglob]
        branches = [b.name for b in project.branches.list(all=True)]
        matching = [b for b in branches if fnmatch.fnmatch(b, filterglob)]
        return matching

    def poke(
        self,
        branch,
        mr=False,
        statuses=None,
        action="retry",
        variables={},
        dry_run=False,
    ):
        Failure = collections.namedtuple("Failure", "project pipeline mr")
        for project in self.projects:
            failures = []
            if branch:
                branches = self._matching_branches(project, branch)
            else:
                branches = [project.default_branch]
            if not mr:
                pipelines = []
                for branch in branches:
                    pipelines += project.pipelines.list(
                        scope="branches", ref=branch, per_page=1
                    )
                pipelines = [p for p in pipelines if p.status in statuses]
                for p in pipelines:
                    failures.append(Failure(project, p, None))
            else:
                mrs = project.mergerequests.list(scope="all", state="opened", all=False)
                mrs = (mr for mr in mrs if mr.target_branch in branches)
                for mr in mrs:
                    pipelines = mr.pipelines.list(
                        status="failed", per_page=1, get_all=False
                    )
                    if not pipelines:
                        continue
                    p = pipelines[0]
                    if p.status == "skipped":
                        continue
                    if p.project_id != project.id:
                        continue
                    pipeline = project.pipelines.get(p.id)
                    failures.append(Failure(project, pipeline, mr))

            for failure in failures:
                pipeline = failure.pipeline
                mr = failure.mr
                pipeline_web_url = pipeline.web_url
                if action == "delete":
                    logging.info(f"Deleting {pipeline_web_url}")
                    if not dry_run:
                        pipeline.delete()
                if action == "cancel":
                    logging.info(f"Cancelling {pipeline_web_url}")
                    if not dry_run:
                        pipeline.cancel()
                if action == "retry":
                    logging.info(f"Retrying {pipeline_web_url}")
                    if not dry_run:
                        pipeline.retry()
                    children = pipeline.bridges.list()
                    for child in children:
                        downstream = child.downstream_pipeline
                        if not downstream:
                            continue
                        child_pipeline = self.gl.projects.get(
                            downstream["project_id"], lazy=True
                        ).pipelines.get(downstream["id"], lazy=True)
                        if not dry_run:
                            child_pipeline.retry()
                if action == "retrigger":
                    logging.info(f"Retriggering {pipeline_web_url}")
                    if dry_run:
                        continue
                    if mr:
                        new_pipeline = mr.pipelines.create()
                    else:
                        varlist = [
                            {"key": key, "value": value}
                            for key, value in variables.items()
                        ]
                        new_pipeline = project.pipelines.create(
                            {
                                "ref": branch,
                                "variables": varlist,
                            }
                        )
                    logging.info(
                        f"Retriggered {new_pipeline.web_url} for {pipeline_web_url}"
                    )


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--debug",
        action="store_const",
        dest="loglevel",
        const=logging.DEBUG,
        help="print debug information",
    )
    parser.add_argument(
        "--quiet",
        action="store_const",
        dest="loglevel",
        const=logging.WARNING,
        help="do not print informational output",
    )
    parser.add_argument(
        "--merge-requests",
        action="store_true",
        help="load MR pipelines rather than branch pipelines",
    )
    parser.add_argument(
        "--dry-run",
        action="store_true",
        help="do not actually poke pipelines",
    )
    parser.add_argument(
        "--gitlab-instance",
        type=str,
        default="apertis",
        help="get connection parameters from this configured instance",
    )
    parser.add_argument(
        "--filter",
        type=str,
        default="pkg/*",
        help="only process projects matching the specified path glob",
    )
    parser.add_argument(
        "--branch",
        type=str,
        help="the branch to retrigger, use the default branch if not specified",
    )
    parser.add_argument(
        "--after",
        type=str,
        help="start processing after the specified project",
    )
    parser.add_argument(
        "--action",
        type=str,
        default="retry",
        choices=["retry", "retrigger", "delete", "cancel"],
        help="'retry' failed jobs, 'retrigger' a whole new pipeline or 'delete' the failed one, or 'cancel' pending and running ones",
    )
    parser.add_argument(
        "--statuses",
        type=str,
        default="failed",
        help="comma-separated list of pipeline statuses to be acted upon, defaults to 'failed'",
    )
    parser.add_argument(
        "--var",
        type=str,
        default=[],
        action="append",
        help="set CI environment variables when retriggering, e.g. FOO=bar",
    )
    args = parser.parse_args()

    logging.basicConfig(level=args.loglevel or logging.INFO)

    variables = dict(v.split("=", 1) for v in args.var)
    statuses = set(args.statuses.split(","))

    runner = Poker()
    runner.connect(args.gitlab_instance)
    runner.fetch_projects(args.filter, archived=False, query_data={"simple": True})
    runner.poke(
        args.branch,
        mr=args.merge_requests,
        action=args.action,
        statuses=statuses,
        variables=variables,
        dry_run=args.dry_run,
    )
