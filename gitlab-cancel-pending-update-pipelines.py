#!/usr/bin/python3
import logging
import sys

import gitlab
import requests  # noqa: F401, needed to get the logger

logging.basicConfig()
logging.getLogger().setLevel(logging.DEBUG)
req_log = logging.getLogger("requests.packages.urllib3")
req_log.setLevel(logging.DEBUG)
req_log.propagate = True

gl = gitlab.Gitlab.from_config("apertis")
gl.auth()

project_full_path = sys.argv[1]

project = gl.projects.get(project_full_path, lazy=True)

pipelines = project.pipelines.list(all=True)
pipelines.sort(key=lambda x: x.id)

pending_update_pipelines = [
    p
    for p in pipelines
    if p.status in ("pending", "created") and p.ref == "debian/buster-gitlab-update-job"
]

if not pending_update_pipelines:
    print(f"{project_full_path} no pipelines")
else:
    print(f"{project_full_path} keep {pending_update_pipelines[0].web_url}")
    for p in pending_update_pipelines[1:]:
        print(f"{project_full_path} cancel {p.web_url} {p.status}")
        p.cancel()
