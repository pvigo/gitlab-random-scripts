#!/usr/bin/env python3

import argparse
import datetime
import logging

import gitlab

STATUSES = {
    "failed": "🛑",
    "success": "✅",
    "skipped": "……",
}


class Lister:
    def __init__(self):
        self.gl = None
        self.project = None
        self.pipelines = []
        self.jobs = {}

    def connect(self, gitlab_instance):
        logging.info(f'Connecting to the "{gitlab_instance}" configured instance')
        self.gl = gitlab.Gitlab.from_config(gitlab_instance)
        self.gl.auth()

    def fetch(self, project_name):
        self.project = self.gl.projects.get(project_name)
        self.pipelines = self.project.pipelines.list(status="failed", all=True)
        for pipeline in self.pipelines:
            jobs = pipeline.jobs.list(all=True)
            jobs.sort(key=lambda j: j.created_at)
            self.jobs[pipeline.id] = jobs

    def report(self):
        def d(date_str):
            date = datetime.datetime.fromisoformat(date_str.rstrip("Z"))
            return date.date().isoformat()

        def t(date_str):
            if not date_str:
                return ""
            date = datetime.datetime.fromisoformat(date_str.rstrip("Z"))
            return date.time().isoformat("seconds")

        for pipeline in self.pipelines:
            print(pipeline.ref, pipeline.web_url, pipeline.created_at)
            for job in self.jobs[pipeline.id]:
                status = STATUSES.get(job.status, "?")
                if job.status in ("skipped",):
                    print(
                        " ",
                        job.id,
                        status,
                        "create=" + t(job.created_at),
                        job.name,
                    )
                else:
                    print(
                        " ",
                        job.id,
                        status,
                        "create=" + (t(job.created_at) if job.created_at else ""),
                        "start=" + (t(job.started_at) if job.started_at else ""),
                        "end=" + (t(job.finished_at) if job.finished_at else ""),
                        "duration=" + (f"{job.duration:.2f}" if job.duration else ""),
                        "queued="
                        + (f"{job.queued_duration:.2f}" if job.queued_duration else ""),
                        job.name,
                    )


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--debug",
        action="store_const",
        dest="loglevel",
        const=logging.DEBUG,
        help="print debug information",
    )
    parser.add_argument(
        "--quiet",
        action="store_const",
        dest="loglevel",
        const=logging.WARNING,
        help="do not print informational output",
    )
    parser.add_argument(
        "--gitlab-instance",
        type=str,
        default="apertis",
        help="get connection parameters from this configured instance",
    )
    parser.add_argument(
        "--project",
        type=str,
        required=True,
        help="list failed jobs on this project",
    )
    args = parser.parse_args()

    logging.basicConfig(level=args.loglevel or logging.INFO)

    lister = Lister()
    lister.connect(args.gitlab_instance)
    lister.fetch(args.project)
    lister.report()
